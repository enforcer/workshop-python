#!/bin/bash

for virtualenv_command in "pyvenv-3.5" "pyvenv-3.4" "pyvenv-3.3" "virtualenv"
do
    if type $virtualenv_command >/dev/null 2>&1; then
        echo "Creating virtualenv ve/ with $virtualenv_command"
        $virtualenv_command ve/
        if type pip >/dev/null 2>&1; then
            pip install -r requirements.txt
        fi
        break
    else
	echo "nie masz $virtualenv_command"
    fi
done

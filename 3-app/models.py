import json
from sqlalchemy import Column, INTEGER, TIMESTAMP, DATETIME, VARCHAR, ForeignKey, Table
from sqlalchemy.orm import relationship, backref
from db import Base
import datetime

room_has_reservation = Table('rooms_have_reservations', Base.metadata,
    Column('rooms_id', INTEGER(), ForeignKey('rooms.id')),
    Column('reservations_id', INTEGER(), ForeignKey('reservations.id'))
)


class Room(Base):
    __tablename__ = 'rooms'

    id = Column(INTEGER(), primary_key=True, nullable=False, autoincrement=True)
    floor = Column(INTEGER(), nullable=False, default=0)
    number = Column(INTEGER(), nullable=False)
    price_per_day = Column(INTEGER(), nullable=False)

    def as_dict(self, omit_children=False):
        result = {
            'id': self.id,
            'floor': self.floor,
            'number': self.number,
            'price_per_day': self.price_per_day
        }
        if not omit_children:
            result['reservations'] = [reservation.as_dict(omit_children=True) for reservation in self.reservations]
        return result

    def is_free(self):
        return len([r for r in self.reservations if r.cancelled_at is None]) == 0


class Reservation(Base):
    __tablename__ = 'reservations'
    required_fields = ('reserver_names', 'arrival_at', 'departure_at', 'room_id')

    CLIMATE_TAX_PER_DAY = 1

    id = Column(INTEGER(), primary_key=True, nullable=False, autoincrement=True)
    reserver_names = Column(VARCHAR(length=255), nullable=False)
    created_at = Column(TIMESTAMP(), nullable=False, default=datetime.datetime.now)
    confirmed_at = Column(TIMESTAMP(), nullable=True)
    arrival_at = Column(DATETIME(), nullable=False)
    departure_at = Column(DATETIME(), nullable=False)
    cancelled_at = Column(DATETIME(), nullable=True)

    room = relationship("Room", secondary=room_has_reservation, uselist=False, backref=('reservations'))

    def as_dict(self, omit_children=False):
        result = {
            'id': self.id,
            'reserver_names': self.reserver_names,
            'created_at': format_datetime(self.created_at),
            'confirmed_at': format_datetime(self.confirmed_at),
            'arrival_at': format_datetime(self.arrival_at),
            'departure_at': format_datetime(self.departure_at),
            'cancelled_at': format_datetime(self.cancelled_at)
        }
        if not omit_children:
            result['room'] = self.room.as_dict(omit_children=True) if self.room is not None else None
        return result

    def calculate_price(self):
        days = (self.departure_at - self.arrival_at).days
        price = days * self.room.price_per_day
        if days > 2:
           price += days * self.CLIMATE_TAX_PER_DAY
        return price


def format_datetime(datetime):
    return datetime.strftime('%Y-%m-%d %H:%I:%S') if datetime is not None else None


def to_datetime(text):
    return datetime.datetime.strptime(text, '%Y-%m-%d %H:%I:%S')


def for_json(obj):
    if hasattr(obj, '__iter__'):
        return [for_json(item) for item in obj]
    else:
        return obj.as_dict()

import unittest
from charge import ChargeExternalService, ChargeResponseError


config = {
    'ACCESS_TOKEN': '75df646b9a0c8e3b81ff839f58cdc983',
}


class ChargeServiceTest(unittest.TestCase):

    def test_invalid_access_token(self):
        service = ChargeExternalService(access_token='123')
        with self.assertRaises(ChargeResponseError):
            service.charge(card_number='irrelevant', card_security_code='123', amount=100)


if __name__ == '__main__':
    unittest.main()

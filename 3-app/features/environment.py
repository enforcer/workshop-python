from db import init_db, clear_db


def before_scenario(context, scenario):
    init_db()

def after_scenario(context, scenario):
    clear_db()
    if hasattr(context, 'freezer'):
        context.freezer.stop()

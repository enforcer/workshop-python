Feature: One can make a reservation of a room specifying arrival and departure date

 Background: Assuming following system state
    Given There are rooms
     | id | floor | number | price_per_day |
     |  8 |     2 |    200 |           100 |
     |  9 |     2 |    201 |           100 |

Scenario: One can make a reservation of free rooms
 Given Room with ID "8" is available
  When "John Doe" reserves room "8" from "2015-10-10" to "2015-10-16"
  Then Reservation for room "8" is made
   And Room with ID "8" is not available

Scenario: One can cancel reservation a week before start
 Given Today is "2015-10-02"
  When "John Doe" reserves room "9" from "2015-10-10" to "2015-10-16"
   And One cancells reservation
  Then Room with ID "9" is available

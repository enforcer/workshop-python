from behave import given, when, then

from db import session
from models import Room
from domain.services import FreeRoomsService, ReservationsService

from datetime import datetime
from freezegun import freeze_time


@given(u'There are rooms')
def step_impl(context):
    for row in context.table:
        session.add(Room(id=row['id'], floor=row['floor'], number=row['number'], price_per_day=row['price_per_day']))
    session.commit()


@given(u'Room with ID "{rooms_id}" is available')
def step_impl(context, rooms_id):
    room = FreeRoomsService.find_by_id(rooms_id)
    assert room is not None


@when(u'"{reserver_names}" reserves room "{rooms_id}" from "{arrival_at}" to "{departure_at}"')
def step_impl(context, reserver_names, rooms_id, arrival_at, departure_at):
    raw_data = {
        'reserver_names': reserver_names,
        'rooms_id': rooms_id,
        'arrival_at': datetime.strptime(arrival_at, '%Y-%m-%d'),
        'departure_at': datetime.strptime(departure_at, '%Y-%m-%d')
    }
    context.last_reservation_attempt_result = ReservationsService.make_reservation(raw_data)


@then(u'Room with ID "{rooms_id}" is not available')
def step_impl(context, rooms_id):
    room = FreeRoomsService.find_by_id(rooms_id)
    assert room is None


@then(u'Reservation for room "{rooms_id}" is made')
def step_impl(context, rooms_id):
    assert context.last_reservation_attempt_result is not None


@given(u'Today is "{date}"')
def step_impl(context, date):
    context.freezer = freeze_time(date)
    context.freezer.start()


@when(u'One cancells reservation')
def step_impl(context):
    ReservationsService.cancel_reservation_by_id(context.last_reservation_attempt_result.id)

@then(u'Room with ID "{rooms_id}" is available')
def step_impl(context, rooms_id):
    room = FreeRoomsService.find_by_id(rooms_id)
    assert room is not None

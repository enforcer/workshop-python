from flask import Flask, jsonify, request, abort
from domain.services import FreeRoomsService, ReservationsService
from domain.exceptions import ReservationNotFound, ReservationAlreadyCancelled, InvalidReservation, RoomUnavailable
from models import for_json, to_datetime


app = Flask(__name__)


@app.route('/free_rooms')
def get_all_rooms():
    rooms = FreeRoomsService.get_all_free_rooms()
    return jsonify({'rooms': for_json(rooms)})


@app.route('/free_rooms/<int:rooms_id>')
def get_room(rooms_id):
    room = FreeRoomsService.find_by_id(rooms_id)
    if room is None:
        abort(404)
    else:
        return jsonify(for_json(room))


@app.route('/reservations', methods=['POST'])
def make_reservation():
    try:
        pass

        reservation_raw_data = {
             'reserver_names': request.form['reserver_names'],
            'arrival_at': to_datetime(request.form['arrival_at']),
            'departure_at': to_datetime(request.form['departure_at']),
            'rooms_id': int(request.form['rooms_id'])
        }
        reservation = ReservationsService.make_reservation(reservation_raw_data)
        return jsonify(for_json(reservation))
    except (KeyError, ValueError):
        abort(400)
    except InvalidReservation:
        abort(422)
    except RoomUnavailable:
        abort(409)


@app.route('/reservations/<int:reservations_id>', methods=['DELETE'])
def cancel_reservation(reservations_id):
    try:
        reservation = ReservationsService.cancel_reservation_by_id(reservations_id)
        return jsonify(for_json(reservation))
    except ReservationNotFound:
        abort(404)
    except ReservationAlreadyCancelled:
        abort(403)


@app.route('/reservations/<int:reservations_id>', methods=['GET'])
def get_reservation_details(reservations_id):
    reservation = ReservationsService.find_by_id(reservations_id)
    if reservation is None:
        abort(404)
    else:
        return jsonify(for_json(reservation))


@app.before_first_request
def init_data():
    from db import init_db
    init_db()


if __name__ == '__main__':
    app.run(debug=True)

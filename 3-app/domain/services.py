import datetime
from repositories import RoomsRepository, ReservationRepository
from models import Reservation
from db import transaction, session
from domain.exceptions import ReservationAlreadyCancelled, ReservationNotFound, InvalidReservation, RoomUnavailable
from domain.validation import NewReservationValidator


class FreeRoomsService(object):

    @classmethod
    def get_all_free_rooms(cls):
        repository = RoomsRepository()
        return repository.find_all_free()

    @classmethod
    def find_by_id(cls, rooms_id):
        repository = RoomsRepository()
        return repository.find_by_id(rooms_id)


class ReservationsService(object):

    @classmethod
    def find_by_id(cls, reservations_id):
        repository = ReservationRepository()
        return repository.find_by_id(reservations_id)

    @classmethod
    @transaction
    def cancel_reservation_by_id(cls, reservations_id):
        reservation = cls.find_by_id(reservations_id)
        if reservation is None:
            raise ReservationNotFound

        if reservation.cancelled_at is not None:
            raise ReservationAlreadyCancelled

        reservation.cancelled_at = datetime.datetime.now()
        return reservation

    @classmethod
    @transaction
    def make_reservation(cls, raw_data):
        rooms_id = raw_data.pop('rooms_id')
        reservation = Reservation(**raw_data)
        if not NewReservationValidator.is_valid(reservation):
            raise InvalidReservation

        requested_room = FreeRoomsService.find_by_id(rooms_id)
        if requested_room is None:
            raise RoomUnavailable

        reservation.room = requested_room
        session.add(reservation)
        return reservation

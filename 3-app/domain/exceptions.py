class ReservationNotFound(Exception):
    pass


class ReservationAlreadyCancelled(Exception):
    pass


class InvalidReservation(Exception):
    pass


class RoomUnavailable(Exception):
    pass

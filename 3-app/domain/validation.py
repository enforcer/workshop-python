import six


class NewReservationValidator(object):
    EMPTY_FIELDS = ('confirmed_at', 'cancelled_at')

    @classmethod
    def is_valid(cls, reservation):
        if reservation.departure_at <= reservation.arrival_at:
            return False
        if any([getattr(reservation, field) is not None for field in cls.EMPTY_FIELDS]):
            return False
        if not isinstance(reservation.reserver_names, six.string_types) or not 3 < len(reservation.reserver_names) <= 255:
            return False
        return True


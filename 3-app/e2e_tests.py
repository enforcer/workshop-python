import os
from app import app
import unittest
import tempfile

from db import session
from models import Room, Reservation
from repositories import RoomsRepository
from datetime import datetime, timedelta


class AppTestCase(unittest.TestCase):

    def setUp(self):
        self.db_fd, app.config['DATABASE'] = tempfile.mkstemp()
        app.config['TESTING'] = True
        self.app = app.test_client()
        from db import init_db; init_db()

    def test_free_rooms(self):
        response = self.app.get('/free_rooms')
        assert response.status_code == 200

    def test_free_room(self):
        response = self.app.get('/free_rooms/2')
        assert response.status_code == 200

    def test_reservation(self):
        response = self.app.get('/reservations/1')
        assert response.status_code == 200

    def test_not_existing_reservation(self):
        response = self.app.get('/reservations/3')
        assert response.status_code == 404

    def test_cancel_not_existing_reservation(self):
        response = self.app.delete('/reservations/3')
        assert response.status_code == 404

    def test_reserved_room_repo(self):
        room = Room(number=101, floor=1, price_per_day=100)
        session.add(room)
        session.commit()

        reservation = Reservation(reserver_names='John Doe', arrival_at=datetime.now(), departure_at=datetime.now() + timedelta(days=3), room=room)
        session.add(reservation)
        session.commit()

        repo = RoomsRepository()
        room2 = repo.find_by_id(room.id)
        assert room2 is None

    def test_cancelled_room_repo(self):
        room = Room(number=102, floor=1, price_per_day=120)
        session.add(room)
        session.commit()

        reservation = Reservation(reserver_names='John Doe', arrival_at=datetime.now(), departure_at=datetime.now() + timedelta(days=3), room=room,
            cancelled_at=datetime.now() + timedelta(days=1))
        session.add(reservation)
        session.commit()

        repo = RoomsRepository()
        room2 = repo.find_by_id(room.id)
        assert room2 is not None

    def test_no_reservations_room_repo(self):
        room = Room(number=103, floor=1, price_per_day=120)
        session.add(room)
        session.commit()

        repo = RoomsRepository()
        room2 = repo.find_by_id(room.id)
        assert room2 is not None

    def tearDown(self):
        os.close(self.db_fd)
        os.unlink(app.config['DATABASE'])


if __name__ == '__main__':
    unittest.main()

from sqlalchemy.pool import StaticPool
from sqlalchemy.engine import create_engine
from sqlalchemy.ext.declarative import declarative_base, DeclarativeMeta
from sqlalchemy.orm import scoped_session, sessionmaker
import datetime
from functools import wraps
import json


engine = create_engine('sqlite:///:memory:', connect_args={'check_same_thread':False}, poolclass=StaticPool)


Base = declarative_base()
session = scoped_session(sessionmaker(bind=engine))


def init_db():
    from models import Room, Reservation
    Base.metadata.create_all(engine)

    clear_db()

    # insert some sample data
    for room_number in range(5):
        room = Room(floor=1, number=100 + room_number, price_per_day=100)
        session.add(room)
    session.commit()

    reservation = Reservation(reserver_names='John Doe', arrival_at=datetime.date.today(), departure_at=datetime.date.today() + datetime.timedelta(days=3), room=room)
    session.add(reservation)
    session.commit()

def clear_db():
    from models import Room, Reservation
    session.query(Room).delete()
    session.query(Reservation).delete()
    session.commit()


def transaction(func):
    @wraps(func)
    def wrapped(*args, **kwargs):
        session.begin(subtransactions=True)
        result = func(*args, **kwargs)
        session.commit()
        return result
    return wrapped



class ReservationValidator(object):
    def __init__(self, reservation):
        self._reservation = reservation

    def is_valid(self):
        return self._reservation.departure_at > self._reservation.arrival_at

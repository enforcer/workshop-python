import requests
import json


class ChargeResponseError(Exception):
    pass


class ChargeResponse(object):
    def __init___(self, success, message):
        self.success = success
        self.message = message


class ChargeExternalService(object):
    URL = 'http://46.101.206.174:6999/charge'
    STATUS_KEY = 'success'
    ERROR_MESSAGE_KEY = 'error'

    def __init__(self, access_token):
        self.access_token = access_token

    def charge(self, card_number, card_security_code, amount):
        response = requests.post(self.URL, data={
            'access_token': self.access_token,
            'card_number': card_number,
            'card_security_code': card_security_code,
            'amount': amount
        })
        decoded = response.json()
        if response.status_code != 200:
            try:
                raise ChargeResponseError(decoded[self.ERROR_MESSAGE_KEY])
            except KeyError:
                raise ChargeResponseError('UnknownError')
        else:
            return ChargeResponse(decoded[self.STATUS_KEY], decoded[self.MESSAGE_KEY])

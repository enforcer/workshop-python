import unittest
import datetime
from models import Room, Reservation


class ReservationPriceCalculation(unittest.TestCase):
    TEST_ROOM_FLOOR = 1
    TEST_ROOM_NUMBER = 100

    def setUp(self):
        self.room = Room(floor=self.TEST_ROOM_FLOOR, number=self.TEST_ROOM_NUMBER)
        self.reservation = Reservation(reserver_names='John Doe', arrival_at=datetime.date.today(),
            departure_at=datetime.date.today() + datetime.timedelta(days=3), room=self.room)

    def test_calculate_price_1_days(self):
        self.verify(days=1, price_per_day=150, expected_price=150)

    def test_calculate_price_3_days(self):
        self.verify(days=3, price_per_day=100, expected_price=303)

    def verify(self, days, price_per_day, expected_price):
        self.room.price_per_day = price_per_day
        self.reservation.arrival_at = datetime.date.today()
        self.reservation.departure_at = datetime.date.today() + datetime.timedelta(days=days)
        self.assertEquals(expected_price, self.reservation.calculate_price())


if __name__ == '__main__':
    unittest.main()

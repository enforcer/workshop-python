from db import session
from models import Room, Reservation, room_has_reservation


class RoomsRepository(object):

    def find_by_id(self, rooms_id):
        room = session.query(Room).get(rooms_id)
        if room is not None and room.is_free():
            return room

    def find_all_free(self):
        all_rooms = session.query(Room).all()
        return [room for room in all_rooms if room.is_free()]


class ReservationRepository(object):

    def find_by_id(self, reservations_id):
        return session.query(Reservation).filter_by(id=reservations_id).first()


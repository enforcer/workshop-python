import unittest

from db import session, User, Role


class DummyEventBroker(object):

    def user_authorized(self, user):
        assert isinstance(user, User)
        # some magic stuff here


class AuthDBAdapter(object):

    def fetch_user_for_credentials(self, login, password):
        """
        Zwraca obiekt usera dla poprawnych danych uwierzytelniajacych
        """
        return session.query(User).filter_by(login=login, password=password).one()


class AuthService(object):

    def __init__(self, adapter, event_broker):
        self._adapter = adapter
        self._event_broker = event_broker

    def authenticate(self, login, password):
        self._validate(login, password)
        user = self._adapter.fetch_user_for_credentials(login, password)
        if user is not None:
            self._event_broker.user_authorized(user)
            return True
        return False

    def _validate(self, login, password):
        if not isinstance(login, str):
            raise TypeError('Login {0} is not a string!'.format(login))
        if not 3 < len(login) < 40:
            raise Exception('Login {0} is too long or too short!'.format(login))
        if not isinstance(password, str):
            raise TypeError('Password {0} is not a string!'.format(password))
        if not 3 < len(password) < 40:
            raise Exception('Password {0} is too long or too short!'.format(password))


class CoupledTest(unittest.TestCase):

    def test_auth_service_existing_user(self):
        adapter = AuthDBAdapter()
        broker = DummyEventBroker()
        service = AuthService(adapter, broker)

        result = service.authenticate('jacek', 'dubilas')

        self.assertTrue(result)

    def test_event_was_send(self):
        self.skipTest('Not implemented yet')


if __name__ == '__main__':
    unittest.main()


import unittest

from db import session, User, Role
from db import admin_role, moderator_role, login_role


def copy_roles(src_user, dst_user):
    pass


class DBCoupledTest(unittest.TestCase):
    TEST_USERS_LOGINS = ('user1', 'user2')

    def clear_db(self):
        session.query(User).filter(User.login.in_(self.TEST_USERS_LOGINS)).delete(synchronize_session='fetch')
        session.commit()

    def setUp(self):
        self.clear_db()

        self.user = User(login=self.TEST_USERS_LOGINS[0], email='someaddress@com.pl', password='haslo',
            roles=[admin_role, moderator_role, login_role])
        self.another_user = User(login=self.TEST_USERS_LOGINS[1], email='another@com.pl', password='123!', roles=[login_role])

        session.add(self.user)
        session.add(self.another_user)
        session.commit()

    def test_copy_roles(self):
        copy_roles(self.user, self.another_user)
        self.assertEquals(3, len(self.another_user.roles))

    def tearDown(self):
        self.clear_db()


if __name__ == '__main__':
    unittest.main()



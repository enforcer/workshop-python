from sqlalchemy import Column, INTEGER, TIMESTAMP, VARCHAR, ForeignKey, Table
from sqlalchemy.engine import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import scoped_session, sessionmaker, relationship
engine = create_engine('sqlite:///:memory:')

Base = declarative_base()
session = scoped_session(sessionmaker(bind=engine))

import datetime


user_has_role = Table('users_have_roles', Base.metadata,
    Column('users_id', INTEGER(), ForeignKey('users.id')),
    Column('roles_id', INTEGER(), ForeignKey('roles.id'))
)


class User(Base):
    __tablename__ = 'users'

    id = Column(INTEGER(), primary_key=True, nullable=False, autoincrement=True)
    login = Column(VARCHAR(length=45), nullable=False)
    email = Column(VARCHAR(length=45), nullable=False)
    password = Column(VARCHAR(length=45), nullable=False)
    created_at = Column(TIMESTAMP(), nullable=False, default=datetime.datetime.now)

    roles = relationship("Role", secondary=user_has_role)

    def has_role(self, role_name):
        return len([role for role in self.roles if role.name == role_name]) > 0


class Role(Base):
    __tablename__ = 'roles'

    id = Column(INTEGER(), primary_key=True, nullable=False, autoincrement=True)
    name = Column(VARCHAR(length=45), nullable=False, unique=True)


Base.metadata.create_all(engine)

"""
Create basic roles
"""
login_role = Role(name='login')
admin_role = Role(name='admin')
moderator_role = Role(name='moderator')
session.add(admin_role)
session.add(moderator_role)
session.add(login_role)
session.commit()

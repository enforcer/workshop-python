"""
Uruchamianie zestawu testow:
  python 1-kata.py
"""
import unittest


class Game(object):

    def roll(self, pins):
        pass

    def score(self):
        pass


class BowlingGameTestCase(unittest.TestCase):
    def test_one(self):
        game = Game()

        for i in range(20):
            game.roll(0)

        self.assertEquals(0, game.score())


if __name__ == '__main__':
    unittest.main()

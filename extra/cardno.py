from flask import Flask, jsonify, request, abort
from time import sleep
from random import randint


app = Flask(__name__)


valid_data = {
    'card_number': '4556727745254415',
    'card_security_code': '161'
}
VALID_ACCESS_TOKEN = '75df646b9a0c8e3b81ff839f58cdc983'
REQUIRED_FIELDS = ('access_token', 'card_number', 'card_security_code', 'amount')


@app.route('/charge', methods=['POST'])
def charge():
    if not all([field in request.form for field in REQUIRED_FIELDS]):
        return jsonify({'success': False, 'error': 'Not all required data was sent'}), 400

    f = request.form
    if f['access_token'] != VALID_ACCESS_TOKEN:
        return jsonify({'success': False, 'error': 'Invalid access token, access denied'}), 403

    sleep(randint(3, 10))
    if all([f[v] == valid_data[v] for v in valid_data.keys()]):
        return jsonify({'success': True, 'message': 'Charged successfully'})
    else:
        return jsonify({'success': False, 'message': 'Charge was unsuccessful'})


if __name__ == '__main__':
    app.run(debug=True)
